using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AStateMachine : MonoBehaviour
{
    protected AState State;

    public void SetState(AState state)
    {
        State = state;
        StartCoroutine(State.Start());
    }
}