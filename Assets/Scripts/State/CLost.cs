using UnityEngine;
using System.Collections;

public class CLost : AState
{
    public CLost(CNpc npc) : base(npc)
    {
    }
    
    public override IEnumerator Start()
    {
        Debug.Log("NPC have lost !");
        
        yield return new WaitForSeconds(1f);

        Npc.SetState(new CNPCAction(Npc));
    }
}