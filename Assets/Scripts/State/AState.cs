﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AState
{
    protected CNpc Npc;

    public AState(CNpc npc)
    {
        Npc = npc;
    }


    public virtual IEnumerator Start()
    {
        yield break;
    }

    public virtual IEnumerator Attack()
    {
        yield break;
    }

    public virtual IEnumerator Search()
    {
        yield break;
    }

    public virtual IEnumerator Collect()
    {
        yield break;
    }
}
