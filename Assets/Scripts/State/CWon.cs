using UnityEngine;
using System.Collections;

public class CWon : AState
{
    public CWon(CNpc npc) : base(npc)
    {
    }

    public override IEnumerator Start()
    {
        Debug.Log("NPC have won !");

        yield return new WaitForSeconds(1f);

        Npc.SetState(new CNPCAction(Npc));
    }
}