using UnityEngine;
using System.Collections;

/*
 * En fonction de l'endroit ou l'objet cliquer par le joueur,
 * lance une des 4 action possible :
 * Start    :   au lancement du state IDLE
 * Attack   :   si le click touche un ennemi;
 * Search   :   si le click touche une zone ou un minerai 
 *              mais que la distance est trop grande;
 * Collect  :   si le click touche une zone ou un minerai 
 *              et que la distance est plus petite que la dst de minage
*/
public class CNPCAction : AState
{
    public CNPCAction(CNpc npc) : base(npc)
    {
    }

    public override IEnumerator Start()
    {
        Debug.Log("NPC is idling, Choose an action.");
        yield break;
    }

    public override IEnumerator Attack()
    {
        bool isDead = Npc.TARGET.Damage(Npc.m_fDamage);
        yield return new WaitForSeconds(1f);

        if (!isDead)
        {
            Npc.SetState(new CNPCAction(Npc));
        }
        else
        {
            Debug.Log("Attack this enemy !");
            Npc.SetState(new CWon(Npc));
        }
    }

    public override IEnumerator Search()
    {
        Debug.Log("Searching for...");
        // va dans la direction indiquer par le joueur

        // si il est arrivé lance une nouvelle action
        {
            yield return new WaitForSeconds(1f);
            Npc.SetState(new CNPCAction(Npc));
        }            
    }

    public override IEnumerator Collect()
    {
        Debug.Log("Collecting...");
        // collect les materiaux sur l'objet

        // si il a terminé de collecter
        {
            yield return new WaitForSeconds(1f);
            Npc.SetState(new CNPCAction(Npc));
        }
    }
}