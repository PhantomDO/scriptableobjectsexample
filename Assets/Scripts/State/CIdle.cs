using System.Collections;
using UnityEngine;

public class CIdle : AState
{
    public CIdle(CNpc npc) : base(npc)
    {
    }

    public override IEnumerator Start()
    {
        Debug.Log("NPC is idling");

        yield return new WaitForSeconds(1f);

        Npc.SetState(new CNPCAction(Npc));
    }
}
