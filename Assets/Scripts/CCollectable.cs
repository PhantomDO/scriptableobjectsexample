﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Collectable", menuName = "Collectable/Wood")]
public class CWood : ACollectable
{
    CWood() { m_strType = "Wood"; }
}

[CreateAssetMenu(fileName = "Collectable", menuName = "Collectable/Iron")]
public class CIron : ACollectable
{
    CIron() { m_strType = "Iron"; }
}

