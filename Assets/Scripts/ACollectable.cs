﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ACollectable : ScriptableObject
{
    protected string m_strType;
    public Image m_imgIcon;
    public GameObject m_goModel;
    public int m_iAmountOfRessources;
}
