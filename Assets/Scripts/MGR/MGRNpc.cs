using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct TypeOfNpcs
{
    public CNpc type;
    public GameObject prefab;
}

[System.Serializable]
public struct NPCS
{
    public int ID;
    public CNpc TYPE;
    public GameObject GO;

    public NPCS(int id, CNpc type, GameObject gameObject)
    {
        ID = id;
        TYPE = type;
        GO = gameObject;
    }
}

public class MGRNpc : MonoBehaviour
{
    #region SINGLETON

    private static MGRNpc m_pInstance = null;
    public static MGRNpc Instance 
    { 
        get => m_pInstance; 
    }

    private void Awake()
    {
        //If instance does not exist in the world
        if (m_pInstance == null)
        {
            //if not, set instance to this
            m_pInstance = this;
        }    
        //If instance already exists and it's not this:
        else if (m_pInstance != this)
        {
            //Then destroy this. This enforces our singleton pattern, 
            //meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);
        }        
    }

    #endregion

    #region NPCS

    public int npcToDestroy = 0;
    [Range(0, 100)]
    public int m_iNumberOfNpcs = 0;
    public List<TypeOfNpcs> m_lTypeNpcs = new List<TypeOfNpcs>();

    private int m_iIndexID = 0;
    private List<NPCS> m_lNpcs = new List<NPCS>();

    public void AddNpc(TypeOfNpcs npc)
    {
        
        // Instantiate a new npc and stock it in the mgr list for npcs
        {
            NPCS newNpc = new NPCS(
                m_iIndexID++, 
                npc.type, 
                Instantiate(npc.prefab, transform.position, Quaternion.identity)
            );
            
            m_lNpcs.Add(newNpc);
        }
    }

    public void DelNpc(int id)
    {
        // Delete one npc in the mgrnpcs list and destroy is related gameobject
        if (id <= m_iIndexID && m_lNpcs.Count != 0)
        {
            NPCS objectToDestroy = m_lNpcs[id];

            //modify the id number of the npc to reorder the list
            for (int index = id + 1; index < m_iIndexID; index++)
            {
                NPCS ptr = m_lNpcs[index]; 
                m_lNpcs[index] = new NPCS(ptr.ID - 1, ptr.TYPE, ptr.GO);
            }

            m_lNpcs.RemoveAt(id);
            
            m_iIndexID--;
            Destroy(objectToDestroy.GO);
        }
    }

    #endregion

    private void Start()
    {
        if (m_lTypeNpcs.Count > 0)
        {
            for (int i = 0; i < m_iNumberOfNpcs; i++)
            {
                AddNpc(m_lTypeNpcs[0]);
            }
        }
    }

    private void Update() 
    {
        // Test des fonction de spawn
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            AddNpc(m_lTypeNpcs[0]);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            AddNpc(m_lTypeNpcs[1]);
        }
        else if (Input.GetKeyDown(KeyCode.Delete))
        {
            DelNpc(npcToDestroy);
        }
    }
}