public interface IDamageable<T>
{
    float Life { get; set; }

    void Kill();

    bool Damage(T damage);
}