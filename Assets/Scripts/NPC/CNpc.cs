using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(BoxCollider))]
public class CNpc : AStateMachine, IDamageable<float>
{
    private float m_fLife;

    [Range(1f, 100f)]
    public float m_fMaxLife = 10f;
    
    [Range(1f, 100f)]
    public float m_fDamage = 2f;

    private CNpc m_npcTarget;
    public CNpc TARGET 
    { 
        get => m_npcTarget; 
        set => m_npcTarget = value; 
    }

    private void Start() 
    {
        Life = m_fMaxLife;
        SetState(new CIdle(this));
    }

    #region Execution

    public void OnAttack()
    {
        StartCoroutine(State.Attack());
    }

    public void OnSearch()
    {
        StartCoroutine(State.Search());
    }

    public void OnCollect()
    {
        StartCoroutine(State.Collect());
    }

    #endregion

    #region IDamageable

    public float Life 
    { 
        get => m_fLife; 
        set => m_fLife = value; 
    }

    public void Kill()
    {
        m_fLife = 0;
    }

    public bool Damage(float damage)
    {
        if ((Life - damage) <= 0)
        {
            // IS DEAD
            Kill();
            return true;
        }            
        else
        {
            // NOT DEAD
            m_fLife -= damage;
            return false;
        }
    }

    #endregion
}
